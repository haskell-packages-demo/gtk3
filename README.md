# gtk3

0[R](https://repology.org/project/haskell:gtk3/versions)ArDeFeNiSu Binding to the Gtk+ graphical user interface library, core library of the Gtk2Hs suite

* Consider using [gi-gtk](https://gitlab.com/haskell-packages-demo/gi-gtk)!
* [*Creating a GUI application in Haskell*
  ](https://www.stackbuilders.com/tutorials/haskell/gui-application/)
  2016-06 Mark Karpov
* [*GUI Programming with gtk2hs*
  ](http://book.realworldhaskell.org/read/gui-programming-with-gtk-hs.html)
  Chapter 23 of Real World Haskell
  2008-11
  Bryan O'Sullivan, Don Stewart, and John Goerzen
* [*Gtk2Hs Tutorial*
  ](https://www.muitovar.com/gtk2hs/)
  2008
  Ian Main, Tony Gale, Hans van Thiel, and Alex Tarkovsky